import base64, os
try:
    from Cryptodome.Cipher import AES
    from Cryptodome.Cipher import PKCS1_OAEP
    from Cryptodome.PublicKey import RSA
    from Cryptodome.Hash import SHA256
except ModuleNotFoundError:
    from Crypto.Cipher import AES
    from Crypto.Cipher import PKCS1_OAEP
    from Crypto.PublicKey import RSA
    from Crypto.Hash import SHA256
except:
    print("Check crypto modules")

pubKey = RSA.importKey(base64.b64decode(""))  # PUBLIC KEY RSA BASE64 format


def encrypt_message(message, pubKey):
    key = os.urandom(16)  # its a random key?, always can be static too (len 16 bytes) - check
    nonce = b"\x00" * 12  # IV or Nonce 12 bytes? - check
    encryptor = PKCS1_OAEP.new(pubKey, hashAlgo=SHA256)  # The crypto Hash output/Algo is SHA256? - check
    encrypted = encryptor.encrypt(key)
    cipher = AES.new(mode=AES.MODE_GCM, key=key, nonce=nonce)
    cipher.nonce = nonce
    arr = cipher.encrypt_and_digest(message)
    return base64.b64encode(encrypted), base64.b64encode(arr[0]+arr[1]), key
    # ^ return the message-key, encrypted message (message + tag) and key


def decrypt_message(message, key):
    nonce = b"\x00" * 12  # re check this
    tag = base64.b64decode(message)[-16:]  # last 16 bytes (message + TAG <- this) of encrypted message
    msg = base64.b64decode(message)[:-16]  # all except last 16 bytes (this -> MESSAGE + tag) of encrypted message
    cipher = AES.new(mode=AES.MODE_GCM, key=key, nonce=nonce)
    decrypted = cipher.decrypt_and_verify(msg, tag)
    return decrypted


while True:
    message = input("\nMessage: ").replace("\\", "")  # maybe your POST data?
    message_key, encrypted_message, key = encrypt_message(message.encode(), pubKey)
    print(f"Message-key:\n{message_key.decode('utf8')}")  # maybe a HTTP Header?
    print(f"EncryptedMessage:\n{encrypted_message.decode('utf8')}")  # maybe your encrypted POST data?
    try:
        decrypted_message = decrypt_message(input("EncryptedResponse: "), key)  # maybe the response of POST encrypted?
        print(f"DecryptedResponse:\n{decrypted_message.decode('utf8')}")  # maybe your clear text response from server?
    except:
        print("[-] invalid")
        pass
